﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script para generación de enemigos
public class EnemySpawn : MonoBehaviour {

	public Vector2 spawnArea;		// Area en que los enemigos son creados
	public int enemySpawn;			// Numero de enemigos a crear
	public float spawnTime;			// Tiempo en que los enemigos van a ser generados
	public GameObject enemy;		// Enemigos a instanciar
	private Vector3 spawnSpot;

	void Start () {
		StartCoroutine ("SpawnEnemies", spawnTime);
	}

	IEnumerator SpawnEnemies(float time){
		yield return new WaitForSeconds (time);

		for (int i = 0; i < enemySpawn; i++) {
			spawnSpot = new Vector3 (Random.Range (-spawnArea.x, spawnArea.x), 0.0f, Random.Range (-spawnArea.y, spawnArea.y));
			Instantiate (enemy, spawnSpot, Quaternion.identity);
		}
		//StartCoroutine ("SpawnEnemies", spawnTime);
	}
}
