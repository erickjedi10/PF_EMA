﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoWalking : MonoBehaviour {
	private Animator demoAnimator;			
	// Use this for initialization
	void Start () {
		
		demoAnimator = GetComponent<Animator> ();
		
	}
	
	// Update is called once per frame
	void Update () {

		//demoAnimator.SetFloat ("activate", Input.GetAxis("Vertical"));
		demoAnimator.SetBool ("roar", Input.GetKeyDown (KeyCode.P));
		demoAnimator.SetBool ("jump", Input.GetKeyDown (KeyCode.Space));
		demoAnimator.SetFloat ("speed", Input.GetAxis ("Vertical"));
		demoAnimator.SetFloat ("direction", Input.GetAxis ("Horizontal"));
	}
}
	