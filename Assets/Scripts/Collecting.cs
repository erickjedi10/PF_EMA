﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collecting : MonoBehaviour {

	public int value;
	public float rotateSpeed;
	private AudioSource audioSource;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
	}

	void Update () {
		gameObject.transform.Rotate (Vector3.down * Time.deltaTime * rotateSpeed);
	}

	void OnTriggerEnter() {
		//GetComponent<MeshRenderer> ().enabled = false;
		ItemManager.instance.Collect(value, gameObject);
		audioSource.Play ();
		//Destroy (gameObject, 0.6f);

	}
}
