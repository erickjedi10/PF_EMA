﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ItemManager : MonoBehaviour {

	public static ItemManager instance = null;
	public GameObject scoreTextObject;

	int score;
	Text scoreText;

	// Update is called once per frame
	void Awake() {

		if (instance == null)
			instance = this;
		else if (instance != null)
			Destroy(gameObject);

		scoreText = scoreTextObject.GetComponent<Text> ();
		scoreText.text = "Score: " + score.ToString ();
			
	}

	public void Collect(int passedValue, GameObject passedObject){
		passedObject.GetComponent<Renderer> ().enabled = false;
		passedObject.GetComponent<Renderer> ().enabled = false;
		Destroy (passedObject, 0.6f);
		score = score + passedValue;
		scoreText.text = "Score " + score.ToString();
	
	}
}
