﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goobjet : MonoBehaviour {

	public Transform goal;
	private NavMeshAgent agent;
	private Animator anim;
	private GameObject healtBaer;
	// Use this for initialization

	void Start () {
		agent = GetComponent<NavMeshAgent> ();
		anim = GetComponent<Animator> (); 
		healtBaer = GameObject.Find ("Base");
	}

	// Update is called once per frame
	void Update () {

		agent.destination = goal.position;
		anim.SetFloat ("speed", 0.2f);
	}

	void OnCollisionEnter(Collision coll){

		if (coll.transform.CompareTag ("Player")) {

			//healtBaer.SendMessage ("TakeDamage",15f);
			SceneManager.LoadScene (2);
		}

	}

}
