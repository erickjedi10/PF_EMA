﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Vida : MonoBehaviour {
	public Image health;
	float vida, vidaMax = 100f;
	// Use this for initialization
	void Start () {
		vida = vidaMax;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void TakeDamage(float amount){
		vida = Mathf.Clamp (vida-amount, 0f , vidaMax);
		health.transform.localScale = new Vector2 (vida / vidaMax, 1);
	}
}
