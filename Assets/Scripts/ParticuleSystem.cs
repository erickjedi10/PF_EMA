﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticuleSystem : MonoBehaviour {

	public GameObject fire;

	// Use this for initialization
	void Start () {
		fire.SetActive (false);
	}
	
	// Update is called once per frame
	void OnCollisionEnter () {

		fire.SetActive (true);
	}

	void OnCollisionExit(){ 
	
		fire.SetActive(false);
	}
}
