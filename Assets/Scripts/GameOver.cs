﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

	private Animator anim;
		
	void Start(){
	
		anim = GetComponent<Animator> ();
	
	}

	void OnTriggerEnter(Collider coll) {
		if(coll.CompareTag ("Abyss"))
		anim.SetBool ("die", true);
	//	SceneManager.LoadScene (2);
	}

	void OnTriggerExit(Collider coll) {
		if(coll.CompareTag ("Abyss"))
		SceneManager.LoadScene (2);
	}


			
}
