﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

	public Transform objetivoTarget; // target a seguir
	public Vector3 targetOffset; // distancia camara
	public float moveSpeed = 2.0f; //Velocidad de Camara

	private Transform camTransform; //


	// Use this for initialization
	void Start () {
			
		camTransform = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void LateUpdate () {

		if (objetivoTarget != null)
			camTransform.position = Vector3.Lerp (camTransform.position, 
				objetivoTarget.position + targetOffset,
				moveSpeed * Time.deltaTime);
	}
	public void SetTarget(Transform aTrasnform) {
			
		objetivoTarget = aTrasnform;
	
	}



}
