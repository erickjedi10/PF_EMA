﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class thirdPersonCamera : MonoBehaviour {
	public Transform target;
	public float smooth = 0.3f;
	public float distance = 5.0f;
	private float yvelocity = 0f;
	public float height= 3.5f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		float yAngle = Mathf.SmoothDampAngle (transform.eulerAngles.y, target.eulerAngles.y, ref yvelocity, smooth);

		Vector3 position = target.position;
		position += Quaternion.Euler (0, yAngle, 0) * new Vector3 (0, height, -distance);

		transform.position = position;
		transform.LookAt (target);


	}
}
