﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	private Animator playerAnimator;
	private AnimatorStateInfo stateInfo;
	private float ColliderHeight;
	private float gravityWeight;
	//private float slowTime;

	private CapsuleCollider playerCollider;

	void Start () {
		// Asignación de elementos
		playerAnimator = GetComponent<Animator> ();
		playerCollider = GetComponent<CapsuleCollider> ();

		ColliderHeight = playerCollider.height;		// Respalda altura inicial del collider
	//	slowTime = Time.timeScale;
	}

	void Update () {
		
		stateInfo = playerAnimator.GetCurrentAnimatorStateInfo (0);		// Obtiene información de layer de interés

		if (stateInfo.IsName ("Jump")) {
			playerCollider.height = playerAnimator.GetFloat ("ColliderHeight");
			//Time.timeScale = playerAnimator.GetFloat ("slowTime");
		} else {
			playerCollider.height = ColliderHeight;
		}

		playerAnimator.SetFloat ("speed", Input.GetAxis ("Vertical"));
		playerAnimator.SetFloat ("direction", Input.GetAxis ("Horizontal"));
		playerAnimator.SetBool ("roar", Input.GetKeyDown(KeyCode.E));
		playerAnimator.SetBool ("punch", Input.GetKeyDown(KeyCode.Q));
		if (Input.GetKeyDown (KeyCode.C))
			playerAnimator.SetTrigger ("slide");
		if (Input.GetKeyDown (KeyCode.Space))
			playerAnimator.SetTrigger ("jump");

		if (Input.GetKeyDown (KeyCode.P) && stateInfo.IsName ("Idle"))
			playerAnimator.SetTrigger ("wave");	
	}

	/*void OnTriggerEnter(Collider coll) {
		playerAnimator.SetTrigger ("die");

	}*/

}
