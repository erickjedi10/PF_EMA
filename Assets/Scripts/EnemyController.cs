﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public enum EnemyState{PATROL,CHASING,DIE}

public class EnemyController : MonoBehaviour {

	public Vector2 patrolArea;
	public float updateDestinationTime;
	public LayerMask playerMask;

	private Animator enemyAnimator;
	private AnimatorStateInfo stateInfo;
	private NavMeshAgent agent;
	private Vector3 destination;
	private EnemyState currentState;
	private RaycastHit enemyHit;

	private Transform player;		// Jugador, detectado en raycast

	void Start () {
		enemyAnimator = GetComponent<Animator> ();
		agent = GetComponent<NavMeshAgent> ();
		currentState = EnemyState.PATROL;

		if (currentState == EnemyState.PATROL)
			StartCoroutine ("CalculateDestinationPatrol", updateDestinationTime);
	}

	// Coroutine
	IEnumerator CalculateDestinationPatrol(float timeToWait){
		yield return new WaitForSeconds (timeToWait);
		destination = new Vector3 (Random.Range (-patrolArea.x, patrolArea.x), 0.0f, Random.Range (-patrolArea.y, patrolArea.y));

		// Reinicia la corrutina
		if (currentState == EnemyState.PATROL)
			StartCoroutine ("CalculateDestinationPatrol", updateDestinationTime);
		
		if (currentState == EnemyState.DIE) {
			enemyAnimator.SetTrigger ("wasTouched");
			yield return new WaitForSeconds (2f);
			Destroy (this.gameObject);
		}
	}

	void Update () {
		Debug.DrawLine (transform.position, transform.forward * 15.0f);

		if (Physics.Raycast (transform.position, transform.forward, out enemyHit, 15.0f, playerMask)) {
			currentState = EnemyState.CHASING;
			StopCoroutine ("CalculateDestinationPoint");
			player = enemyHit.transform;			// Detecta al transform del jugador
		}

		// Cuando entra en Chasing continúa persiguiendo al jugador
		if (currentState == EnemyState.CHASING)
			destination = player.transform.position;
		
		if (currentState == EnemyState.DIE)
			agent.SetDestination (transform.position);

		agent.SetDestination (destination);
		enemyAnimator.SetFloat ("speed", agent.velocity.magnitude);
	}

	void OnCollisionEnter(Collision playerCollider){
		if (playerCollider.transform.CompareTag ("Player")) {
			if (currentState != EnemyState.CHASING)
				currentState = EnemyState.DIE;
			else
				SceneManager.LoadScene (0);
		}
		
	}

	/*
	public void CalculateDestinationPatrol(){
		destination = new Vector3 (Random.Range (-patrolArea.x, patrolArea.x), 0.0f, Random.Range (-patrolArea.y, patrolArea.y));
	}
	*/
}
